import type { Dispatch as ReduxDispatch, Action as ReduxAction } from "redux"

export type Dispatch = ReduxDispatch

export type Action = ReduxAction