export interface FavoriteChannel {
	id: string;
	title: string;
}