import type { PictureAttachment, VideoAttachment } from "social-components"

export type SlideshowSlide = PictureAttachment | VideoAttachment;
