export type ChannelLayout =
	'threadsList' |
	'threadsListWithLatestComments' |
	'threadsTiles';