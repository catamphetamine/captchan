# Example

This is just a simple example of a "fake" data source. It doesn't use any external APIs. The data here is "hardcoded". Any changes to the data will be reset on page reload.

To see it in action, go to https://anychans.github.io/example

Also see the docs on [adding a new data source](https://gitlab.com/catamphetamine/anychan/-/blob/master/docs/add-new-data-source.md).