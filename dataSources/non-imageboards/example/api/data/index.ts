import type { ChannelFromDataSource, ThreadFromDataSource, CommentFromDataSource } from '@/types'

import { CHANNEL1 } from './channel1/index.js'

export {
	CHANNEL1,
	CHANNEL1_THREAD1,
	CHANNEL1_THREAD2
} from './channel1/index.js'

export const CHANNELS = [
	CHANNEL1
]

export const IS_UNTOUCHED = true