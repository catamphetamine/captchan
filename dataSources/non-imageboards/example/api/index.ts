import { getTopChannels } from './getTopChannels.js'
import { getThreads } from './getThreads.js'
import { getThread } from './getThread.js'
import { createThread } from './createThread.js'
import { createComment } from './createComment.js'
import { rateComment } from './rateComment.js'
import { reportComment } from './reportComment.js'

export default {
	getTopChannels,
	getThreads,
	getThread,
	createThread,
	createComment,
	rateComment,
	reportComment
}