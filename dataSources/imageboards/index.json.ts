import TwoChannel from './2ch/index.json' assert { type: 'json' }
import FourChan from './4chan/index.json' assert { type: 'json' }
import EightChan from './8ch/index.json' assert { type: 'json' }
import EndChan from './endchan/index.json' assert { type: 'json' }
import KohlChan from './kohlchan/index.json' assert { type: 'json' }
import LainChan from './lainchan/index.json' assert { type: 'json' }
import ArisuChan from './arisuchan/index.json' assert { type: 'json' }
import SmugLoli from './smugloli/index.json' assert { type: 'json' }
import LeftyPol from './leftypol/index.json' assert { type: 'json' }
import TvChan from './tvchan/index.json' assert { type: 'json' }
import VecchioChan from './vecchiochan/index.json' assert { type: 'json' }
import Bandada from './bandada/index.json' assert { type: 'json' }
import TahtaCh from './tahtach/index.json' assert { type: 'json' }
import WizardChan from './wizardchan/index.json' assert { type: 'json' }
import JakpartySoy from './jakparty.soy/index.json' assert { type: 'json' }
import JunkuChan from './junkuchan/index.json' assert { type: 'json' }
import ZzzChan from './zzzchan/index.json' assert { type: 'json' }
import AlogsSpace from './alogs.space/index.json' assert { type: 'json' }
import NinetyFourChan from './94chan/index.json' assert { type: 'json' }
import PtChan from './ptchan/index.json' assert { type: 'json' }
import DioChan from './diochan/index.json' assert { type: 'json' }
import NiuChan from './niuchan/index.json' assert { type: 'json' }
import TwentySevenChan from './27chan/index.json' assert { type: 'json' }
import HeolCafe from './heolcafe/index.json' assert { type: 'json' }

export default [
	TwoChannel,
	FourChan,
	EightChan,
	KohlChan,
	EndChan,
	LainChan,
	ArisuChan,
	TvChan,
	SmugLoli,
	LeftyPol,
	VecchioChan,
	Bandada,
	TahtaCh,
	WizardChan,
	JakpartySoy,
	JunkuChan,
	ZzzChan,
	AlogsSpace,
	NinetyFourChan,
	PtChan,
	DioChan,
	NiuChan,
	TwentySevenChan,
	HeolCafe
] as const