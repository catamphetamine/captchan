import TwoChannel from './2ch/index.js'
import FourChan from './4chan/index.js'
import EightChan from './8ch/index.js'
import EndChan from './endchan/index.js'
import KohlChan from './kohlchan/index.js'
import LainChan from './lainchan/index.js'
import ArisuChan from './arisuchan/index.js'
import SmugLoli from './smugloli/index.js'
import LeftyPol from './leftypol/index.js'
import TvChan from './tvchan/index.js'
import VecchioChan from './vecchiochan/index.js'
import Bandada from './bandada/index.js'
import TahtaCh from './tahtach/index.js'
import WizardChan from './wizardchan/index.js'
import JakpartySoy from './jakparty.soy/index.js'
import JunkuChan from './junkuchan/index.js'
import ZzzChan from './zzzchan/index.js'
import AlogsSpace from './alogs.space/index.js'
import NinetyFourChan from './94chan/index.js'
import PtChan from './ptchan/index.js'
import DioChan from './diochan/index.js'
import NiuChan from './niuchan/index.js'
import TwentySevenChan from './27chan/index.js'
import HeolCafe from './heolcafe/index.js'

export default [
	TwoChannel,
	FourChan,
	EightChan,
	KohlChan,
	EndChan,
	LainChan,
	ArisuChan,
	TvChan,
	SmugLoli,
	LeftyPol,
	VecchioChan,
	Bandada,
	TahtaCh,
	WizardChan,
	JakpartySoy,
	JunkuChan,
	ZzzChan,
	AlogsSpace,
	NinetyFourChan,
	PtChan,
	DioChan,
	NiuChan,
	TwentySevenChan,
	HeolCafe
] as const